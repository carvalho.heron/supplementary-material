#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "gemm_intracluster.h"
#include "hpcshelf.h"

void gemm_node(double alpha, double* a, double* b, double beta, double* c, int M, int P, int N);
void gemm_reduce(int irow, int jcol, int i, Topology* top, double alpha, double beta, double* c, double* c_prime, MPI_Datatype dt, MPI_Op sum_op, MPI_Comm row_comm);
void gemm_shift (int irow, Topology* top, double *b, MPI_Comm col_comm);

void add_double_vector(void *in, void *inout, int *len, MPI_Datatype *dtype)
{
    double *invec = in;
    double *inoutvec = inout;
    int nints, naddresses, ntypes;
    int combiner;

    if (*len != 1) {
        fprintf(stderr,"my_add: len>1 not implemented.\n");
        return;
    } 
   
    MPI_Type_get_envelope(*dtype, &nints, &naddresses, &ntypes, &combiner); 
    if (combiner != MPI_COMBINER_VECTOR) 
    {
        fprintf(stderr,"my_add: do not understand composite datatype.\n");
        return;
    } 

    int vecargs [nints];
    MPI_Aint vecaddrs[naddresses];
    MPI_Datatype vectypes[ntypes];

    MPI_Type_get_contents(*dtype, nints, naddresses, ntypes, 
            vecargs, vecaddrs, vectypes);

    if (vectypes[0] != MPI_DOUBLE) {
        fprintf(stderr,"my_add: not a vector of DOUBLEs.\n");
    }

    int count    = vecargs[0];
    int blocklen = vecargs[1];
    int stride   = vecargs[2];
    
    for ( int i=0; i<count; i++ ) {
        for ( int j=0; j<blocklen; j++) {
            inoutvec[i*stride+j] += invec[i*stride+j]; 
        } 
    }
}

int my_facet_intra = -1;
int my_rank_intra = -1;

// Essa função será utilizada na implementação do componente GEMM.
void gemm_intracluster(int facet_world,
         double alpha,   // constant
         double* a,      // A (concatenated) blocks 
         double* b,      // B (concatenated) blocks
         double beta,    // constant
         double* c,      // C (concatenated) blocks
         double* c_prime,
         Topology* top,  
         MPI_Comm comm
        )   
{
    my_facet_intra = facet_world;

    int rank;
    MPI_Comm_rank(comm, &rank);
    
    my_rank_intra = rank;

    int irow = rank/top->Y; 
    int jcol = rank % top->Y; 

    int M_node   = top->M/top->X;         //40
    int N_node   = top->N/top->Y;         //60
    int P_node_x = top->P/top->X;         //40
    int P_node_y = top->P/top->Y;         //40
  
    printf("%d/%d: INTRA --- row=%d, col=%d\n", facet_world, rank, irow, jcol);
  
    // Create a communicator for processes in the same line.
    MPI_Comm row_comm; 
    MPI_Comm_split(comm, irow, jcol, &row_comm);
   
    printf("%d/%d: GEMM INTRACLUSTER SPLIT ROW\n", facet_world, rank); 
 
     MPI_Comm col_comm; 
    MPI_Comm_split(comm, jcol, irow, &col_comm);

    printf("%d/%d: GEMM INTRACLUSTER SPLIT COL\n", facet_world, rank); 
 
    MPI_Datatype dt; 
    MPI_Type_vector(M_node, top->pc, P_node_y, MPI_DOUBLE, &dt);
    MPI_Type_commit(&dt);

    printf("%d/%d: GEMM INTRACLUSTER TYPE\n", facet_world, rank); 
 
    MPI_Op sum_op;
    MPI_Op_create((MPI_User_function *)add_double_vector, 1, &sum_op);
 
    for (int i=0; i<top->X; i++)
    {
        printf("%d/%d: GEMM INTRACLUSTER 1 i=%d\n", facet_world, rank, i);
        memset(c_prime, 0, M_node*P_node_x*sizeof(double));
        printf("%d/%d: GEMM INTRACLUSTER 2 i=%d\n", facet_world, rank, i);
        gemm_node(alpha, a, b, 1.0, c_prime, M_node, N_node, P_node_x);
        printf("%d/%d: GEMM INTRACLUSTER 3 i=%d\n", facet_world, rank, i);
        gemm_reduce(irow, jcol, i, top, alpha, beta, c_prime, c, dt, sum_op, row_comm); 
        printf("%d/%d: GEMM INTRACLUSTER 4 i=%d\n", facet_world, rank, i);
        gemm_shift(irow, top, b, col_comm);
        printf("%d/%d: GEMM INTRACLUSTER 5 i=%d\n", facet_world, rank, i); 
    }
    
    printf("%d/%d: GEMM INTRACLUSTER 6 \n", facet_world, rank); 

    MPI_Op_free(&sum_op);
    MPI_Type_free(&dt);
}

// multiplicação de matrizes simples ...
void gemm_node(double alpha, double* a, double* b, double beta, double* c, int M, int N, int P)
{
   int i, k, j; 
   double c1, c2;

   for (i = 0; i < M; i++)
   {
     for (j = 0; j < P; j++)
     {
        c1 = *(c + P * i + j) * beta;
        c2 = 0;
        for (k = 0; k < N; k++)
            c2 += *(a + N * i + k) * *(b + N * j + k);
        *(c + P * i + j) = c1 + alpha*c2;
      }
   }
}

// assuming top->mc = top->ma e top->pc=top->pb
void gemm_reduce(int irow, 
                 int jcol, 
                 int i, 
                 Topology* top, 
                 double alpha, 
                 double beta, 
                 double* c_prime, 
                 double* c, 
                 MPI_Datatype dt, 
                 MPI_Op sum_op, 
                 MPI_Comm row_comm)
{
  char message[256];

  int M_node   = top->M/top->X;         //40
  int N_node   = top->N/top->Y;         //60
  int P_node_x = top->P/top->X;         //40
  int P_node_y = top->P/top->Y;         //40

  int root = (i + irow) % top->X;
  
  for (int j=0; j<top->Y; j++)
  {
     printf("%d/%d: GEMM INTRA REDUCE LOOP 1 i=%d j=%d root=%d irow=%d jcol=%d\n", my_facet_intra, my_rank_intra, i, j, root, irow, jcol); 
     if (jcol == (root % top->Y)) 
         for (int ii = 0; ii < M_node; ii++) 
         {
             for (int jj = 0; jj < top->pc; jj++) 
             {  
                  *(c_prime + j*top->pc + (ii*P_node_x) + jj) += beta * *(c + (root/top->Y)*top->pc + (ii*P_node_y) + jj);
             }
         }  

     printf("%d/%d: GEMM INTRA REDUCE LOOP 2 i=%d j=%d root=%d irow=%d jcol=%d\n", my_facet_intra, my_rank_intra, i, j, root, irow, jcol); 
     MPI_Reduce(c_prime + j*top->pc, c + (root/top->Y)*top->pc, 1, dt, sum_op, root % top->Y, row_comm);
     printf("%d/%d: GEMM INTRA REDUCE LOOP 3 i=%d j=%d root=%d irow=%d jcol=%d\n", my_facet_intra, my_rank_intra, i, j, root, irow, jcol); 
     
     root += top->X;
  } 
} 

void gemm_shift (int irow, Topology* top, double *b, MPI_Comm col_comm)
{  
  // Create a communicator for processes in the same column.

  int N_node = top->N/top->Y;         //60
  int P_node = top->P/top->X;         //40

  int count, recipient, sender, tag = 0;   
  count = N_node * P_node;

  recipient = irow == top->X-1 ? 0        : irow + 1;
  sender    = irow == 0        ? top->X-1 : irow - 1;

  printf("%d: send to %d, recv from %d\n", irow, recipient, sender);
  MPI_Status status;
  MPI_Sendrecv_replace(b, count, MPI_DOUBLE, recipient, tag, sender, tag, col_comm, &status);
}
