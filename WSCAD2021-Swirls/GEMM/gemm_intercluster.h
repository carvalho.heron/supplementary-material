#include "mpi.h"
#include "topology.h"

void gemm_intercluster
        (
         double alpha,   // constant
         double* a,       // A (concatenated) blocks 
         double** b,       // B (concatenated) blocks
         double** b_prime,       // B (concatenated) blocks
         double beta,     // constant
         double* c,      // C (concatenated) blocks
         double* c_prime,
         Topology* top_intra,
         Topology* top_inter//,  
//         MPI_Comm comm
        );
