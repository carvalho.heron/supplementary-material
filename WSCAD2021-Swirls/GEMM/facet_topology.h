#include "hpcshelf.h"

typedef struct FacetTopologyType
{
    int facet;
    HPCShelf_Port port;
    int facet_count;
    int* facet_size;
    int* facet_instance;
} FacetTopology;

FacetTopology* getFacetTopology(char* port_name);
void releasePort(FacetTopology* facet_topology);
