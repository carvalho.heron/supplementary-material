#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "gemm_intracluster.h"
#include "gemm_intercluster.h"
#include "blocklist.h"
#include "hpcshelf.h"
#include "facet_topology.h"


void gemm_inter_reduce_C (Topology* top_inter, Topology* top_intra, int i, int row, int col, double alpha, double beta, double*  c, double*  c_prime, HPCShelf_Port row_port, FacetTopology* facet_topology);
void gemm_inter_rotate_B  (Topology* top_inter, Topology* top_intra, int i, int row, int col,                           double** b, double** b_prime, HPCShelf_Port col_port, FacetTopology* facet_topology);

void resolve_topology_shift  (int rank, int pos, int X, FacetTopology* facet_topology, Topology* top_intra, HPCShelf_Port port, BlockList**** config_block);
void resolve_topology_reduce (int rank, int pos, int X, FacetTopology* facet_topology, Topology* top_intra, HPCShelf_Port port, BlockList**** config_block);

void resolve_topology_direction(int rank, 
                                int nr,
                                int nc,
                                int b1r,
                                int b1c,
                                int x1r,
                                int x1c, 
                                int partner_facet,
                                int partner_size, 
                                HPCShelf_Port port, 
                                BlockList*** config_block);
                      
BlockList*** config_block_r;
BlockList*** config_block_c;

int my_facet = -1;
int my_rank = -1;

// Essa função será utilizada na implementação do componente GEMM.
void gemm_intercluster
        (
         double alpha,    // constant
         double* a,       // A (concatenated) blocks 
         double** b,       // B (concatenated) blocks
         double** b_prime,       // B (concatenated) blocks
         double beta,     // constant
         double* c,      // C (concatenated) blocks
         double* c_prime,
         Topology* top_intra,
         Topology* top_inter
        )   
{
    char message[256];

    MPI_Comm comm = MPI_COMM_WORLD;    

    int M_node = top_intra->M/top_intra->X; //40
    int N_node = top_intra->N/top_intra->Y; //60
    int P_node = top_intra->P/top_intra->X; //60

    FacetTopology* facet_topology_row = getFacetTopology("port_0");
    FacetTopology* facet_topology_col = getFacetTopology("port_1");

    MPI_Comm_rank(comm, &my_rank);
    my_facet = facet_topology_row->facet_instance[facet_topology_row->facet];

    HPCShelf_Port row_port = facet_topology_row->port;
    HPCShelf_Port col_port = facet_topology_col->port;
    
    sprintf(message, "port_0 = %d, port_1 = %d rank = %d\n", row_port, col_port, my_rank);  HPCShelf_Browse(message);
    
    int row = facet_topology_col->facet;
    int col = facet_topology_row->facet;

    sprintf(message, "row = %d, col = %d rank = %d\n", row, col, my_rank); HPCShelf_Browse(message);

    resolve_topology_reduce (my_rank, col, top_inter->X, facet_topology_row, top_intra, row_port, &config_block_r);
    resolve_topology_shift  (my_rank, row, top_inter->Y, facet_topology_col, top_intra, col_port, &config_block_c);
        
    for (int i=0; i<top_inter->X; i++)
    {
        sprintf(message, "ITERATION BEGIN %d - row %d, col %d rank = %d", i, row, col, my_rank); HPCShelf_Browse(message);
        gemm_intracluster (my_facet, alpha, a, *b, 1.0, c, c_prime, top_intra, comm);   
        printf("%d/%d: ITERATION 2 - i=%d row %d, col %d \n", my_facet, my_rank, i, row, col); 
        gemm_inter_reduce_C (top_inter, top_intra, i, row, col, alpha, beta, c, c_prime, row_port, facet_topology_row); 
        printf("%d/%d: ITERATION 3 - i=%d row %d, col %d \n", my_facet, my_rank, i, row, col); 
        gemm_inter_rotate_B  (top_inter, top_intra, i, row, col,              b, b_prime, col_port, facet_topology_col);
        sprintf(message, "ITERATION END %d - row %d, col %d rank = %d", i, row, col, my_rank); HPCShelf_Browse(message);
    }
    
    releasePort(facet_topology_row);
    releasePort(facet_topology_col);
}



void resolve_topology_shift(int rank, int pos, int X, FacetTopology* facet_topology, Topology* top_intra, HPCShelf_Port port, BlockList**** config_block)
{
    int facet_count = facet_topology->facet_count;
    int* facet_size = facet_topology->facet_size;
    int* facet_instance = facet_topology->facet_instance;
    
    *config_block = (BlockList***) malloc(facet_count*sizeof(BlockList**));
    for (int i=0; i<facet_count; i++) (*config_block)[i] = NULL;

    int pos_r = pos == X-1 ? 0   : pos + 1;
    int pos_l = pos == 0   ? X-1 : pos - 1;

    printf("%d/%d: RESOLVE TOPOLOGY 1 - BEGIN pos_r=%d\n", my_facet, my_rank, pos_r);
    resolve_topology_direction(rank, top_intra->P, top_intra->N, top_intra->pb, top_intra->n, top_intra->X, top_intra->Y, facet_instance[pos_r], facet_size[pos_r], port, &(*config_block)[pos_r]/*, 0, top_intra->P, 0 top_intra->N*/);
    printf("%d/%d: RESOLVE TOPOLOGY 1 - END pos_r=%d\n", my_facet, my_rank, pos_r);
    if (pos_r != pos_l)
    {
        printf("%d/%d: RESOLVE TOPOLOGY 2 - BEGIN pos_l=%d\n", my_facet, my_rank, pos_l);
        resolve_topology_direction(rank, top_intra->P, top_intra->N, top_intra->pb, top_intra->n, top_intra->X, top_intra->Y, facet_instance[pos_l], facet_size[pos_l], port, &(*config_block)[pos_l]/*, 0, top_intra->P, 0 top_intra->N*/);
       printf("%d/%d: RESOLVE TOPOLOGY 2 - END pos_l=%d\n", my_facet, my_rank, pos_l);
    }
}
                           
void resolve_topology_reduce(int rank, int pos, int X, FacetTopology* facet_topology, Topology* top_intra, HPCShelf_Port port, BlockList**** config_block)
{
    int facet_count = facet_topology->facet_count;
    int* facet_size = facet_topology->facet_size;
    int* facet_instance = facet_topology->facet_instance;
    
    *config_block = (BlockList***) malloc(facet_count*sizeof(BlockList**));
    for (int i=0; i<facet_count; i++)
        (*config_block)[i] = NULL;

    for (int pos_x=0; pos_x < X; pos_x++)
    {
        if (pos_x != pos)
        {
            printf("%d/%d: RESOLVE TOPOLOGY 3 - BEGIN pos_x=%d\n", my_facet, my_rank, pos_x);
            resolve_topology_direction(rank, top_intra->M, top_intra->P, top_intra->ma, top_intra->pb, top_intra->X, top_intra->Y, facet_instance[pos_x], facet_size[pos_x], port, &(*config_block)[pos_x]/*, ?, ?, ?, ?*/);
            printf("%d/%d: RESOLVE TOPOLOGY 3 - END pos_x=%d\n", my_facet, my_rank, pos_x);
        }
    }
}


int concat_blocks(BlockList* first_block)
{
    int change=1;

    while (change)
    {
        BlockList* block = first_block;
        BlockList* last_block = block;
        block = block->next;
    
        change=0;
        while (block)
        {       
            if (last_block->data->x2_local + 1 == block->data->x1_local && last_block->data->y1_local == block->data->y1_local && last_block->data->y2_local == block->data->y2_local)
            {
                last_block->data->x2_local = block->data->x2_local;
                last_block->data->x2_global = block->data->x2_global;                
                last_block->next = block->next;            
                change=1;
            }
            else if (last_block->data->y2_local + 1 == block->data->y1_local && last_block->data->x1_local == block->data->x1_local && last_block->data->x2_local == block->data->x2_local)
            {
                last_block->data->y2_local = block->data->y2_local;
                last_block->data->y2_global = block->data->y2_global;
                last_block->next = block->next;
                change=1;
            }
            else
            {
               last_block = block;
            }
            
            block = block->next;    
        }
    }
}

int max(int x, int y)
{
   if (x > y) return x;
   else return y;
}

void calculate_block_configuration(int n, int b, int x, int* r[], int* l[], int* h[])
{
    int number_of_blocks = n / b;
   
    *r = (int*) malloc(number_of_blocks * sizeof(int));    
    *l = (int*) malloc(number_of_blocks * sizeof(int));    
    *h = (int*) malloc(number_of_blocks * sizeof(int));    

    for (int i=0; i<number_of_blocks; i++)
    {
    	(*r)[i] = i % x;
    	(*l)[i] = i * b;
    	(*h)[i] = (i+1) * b - 1;
    }
}


void align_blocks(int rank, int n, int b1, int b2, int x1, int x2, int* block_l[], int* block_h[], int* block_r[], int* s)
{    
    int *r1, *r2, *l1, *l2, *h1, *h2;
    
    printf("%d/%d: ------------------ rank=%d n=%d b1=%d b2=%d \n", my_facet, my_rank, rank, n, b1, b2);
    
    int number_of_blocks_1 = n / b1;
    int number_of_blocks_2 = n / b2;
    
    calculate_block_configuration(n, b1, x1, &r1, &l1, &h1);
    for (int i=0; i<number_of_blocks_1; i++)
    	printf("%d/%d: l1[%d]=%d / h1[%d]=%d / r1[%d]=%d\n", my_facet, my_rank, i, l1[i], i, h1[i], i, r1[i]);

    calculate_block_configuration(n, b2, x2, &r2, &l2, &h2);
    for (int i=0; i<number_of_blocks_2; i++)
    	printf("%d/%d: l2[%d]=%d / h2[%d]=%d / r2[%d]=%d\n", my_facet, my_rank, i, l2[i], i, h2[i], i, r2[i]);
    
    *block_l = (int*) malloc(number_of_blocks_1 * max(2, b1/b2) * sizeof(int));
    *block_h = (int*) malloc(number_of_blocks_1 * max(2, b1/b2) * sizeof(int));
    *block_r = (int*) malloc(number_of_blocks_1 * max(2, b1/b2) * sizeof(int));

    int j = 0;    
    int i1 = rank;
    int lower1 = l1[i1];
    int higher1;
    int i2 = 0; while (h2[i2] + 1 <= lower1) i2 ++;
    while (i1 < number_of_blocks_1 && i2 < number_of_blocks_2)
    {
		(*block_l)[j] = lower1; 
		(*block_r)[j] = r2[i2];		
    	if (h1[i1] <= h2[i2])
    	{
		    (*block_h)[j] = h1[i1];
    		i1 += x1;
    		lower1 = l1[i1];
    		while (h2[i2] + 1 <= lower1) i2 ++;
    	}
    	else
    	{
		    (*block_h)[j] = h2[i2];
    		lower1 = h2[i2] + 1;
    		i2++;    		
    	}
    	
        printf("%d/%d: (*block_l)[%d]=%d, (*block_h)[%d]=%d, (*block_r)[%d]=%d \n", my_facet, my_rank, j, (*block_l)[j], j, (*block_h)[j], j, (*block_r)[j]);
    	
   		j++;    		
    }
    
    *s = j;
}

void resolve_topology_direction
                             (int rank, 
                              int nr,
                              int nc,
                              int b1r,
                              int b1c,
                              int x1r,
                              int x1c,
                              int partner_facet,
                              int partner_size,
                              HPCShelf_Port port, 
                              BlockList*** config_block)
{
    int* block_config_X_L;
    int* block_config_X_H;
    int* block_config_X_R; 
    int s_X;		 
    int* block_config_Y_L;
    int* block_config_Y_H;
    int* block_config_Y_R; 
    int s_Y;

	int row_rank = rank / x1c;
	int col_rank = rank % x1c;
	
    int config[4] = { b1r, x1r, b1c, x1c };    

	if (rank == 0)
		for (int i=0; i<partner_size; i++)
			HPCShelf_Send(config, 4, MPI_INT, partner_facet, i, 1, port);
		
	HPCShelf_Recv(config, 4, MPI_INT, partner_facet, 0, 1, port);
	 
    align_blocks(row_rank, nr, b1r, config[0], x1r, config[1], &block_config_X_L, &block_config_X_H, &block_config_X_R, &s_X);  
	align_blocks(col_rank, nc, b1c, config[2], x1c, config[3], &block_config_Y_L, &block_config_Y_H, &block_config_Y_R, &s_Y);
               	                      
    *config_block = (BlockList**) malloc(partner_size*sizeof(BlockList*));
    for (int i=0; i<partner_size; i++) (*config_block)[i] = NULL;	                  
               
    int x1_local=0;
    int x2_local=0;
    for (int i=0; i < s_X; i++)
    {
        int x1 = block_config_X_L[i];                                                // x1 = 20           
        int x2 = block_config_X_H[i];                                                // x2 = 39
   		x2_local = x1_local + (x2 - x1);                                             // x2_local = 19
  		int target_row = block_config_X_R[i];                                        // target_row = 1
  		
        int y1_local=0;
        int y2_local=0;
    	for (int j=0; j < s_Y; j++)
    	{
    		int y1 = block_config_Y_L[j];                                            // y1 = 0              40
    		int y2 = block_config_Y_H[j];                                            // y2 = 19             59
    		y2_local = y1_local + (y2 - y1);                                         // y2_local = 19       39
    		int target_col = block_config_Y_R[j];                                    // target_col = 0      2
    		int target_rank = target_row*config[3] + target_col;                     // target_rank = 3     5    
            
            BlockConfig* block = (BlockConfig*) malloc(sizeof(BlockConfig));
            block->x1_local = block->a1 = x1_local;
            block->x2_local = block->a2 = x2_local;
            block->y1_local = block->b1 = y1_local;
            block->y2_local = block->b2 = y2_local;
            block->x1_global = x1;
            block->x2_global = x2;
            block->y1_global = y1;
            block->y2_global = y2;
            block->ignore = 0;
            (*config_block)[target_rank] = insert((*config_block)[target_rank], block);   
            
            printf("%d/%d: BLOCK [%d] partner_facet=%d x1_local=%d x2_local=%d y1_local=%d y2_local=%d / x1=%d x2=%d y1=%d y2=%d \n", my_facet, my_rank, target_rank, partner_facet, x1_local, x2_local, y1_local, y2_local, x1, x2, y1, y2);
               
    		y1_local += (y2 - y1 + 1);                                               // y1_local = 20
    	}
   		x1_local += (x2 - x1 + 1);
    }
    
 //   for (int i=0; i<partner_size; i++) 
 //       if ((*config_block)[i])
 //              concat_blocks((*config_block)[i]);

}

void gemm_inter_comm_perform  (char* sss,
                               int (*HPCShelf_Oper)(int, int, int, BlockList* block, double*, MPI_Datatype, int, int, int, HPCShelf_Port),
                               int P_node, 
                               int N_node,
                               double *b, 
                               MPI_Datatype dt,
                               int partner_facet, 
							   int partner_size,
                               HPCShelf_Port col_port,
                               BlockList*** config_block,
                               int a1,
                               int a2,
                               int b1,
                               int b2)
                               
{                  
    printf("%d/%d: BEGIN - GEMM INTER COMM PERFORM 0 %s partner_facet=%d partner_size=%d a1=%d a2=%d b1=%d b2=%d\n", my_facet, my_rank, sss, partner_facet, partner_size, a1, a2, b1, b2);

    for (int i=0; i<partner_size; i++) 
        if ((*config_block)[i])
        {
            int count = 0;
            BlockList* current_block = (*config_block)[i];
            while (current_block)
            {                
                // check whether the block is outside the limits and must be ignored.
                current_block->data->ignore = current_block->data->x2_global < a1 || 
                                              current_block->data->x1_global > a2 || 
                                              current_block->data->y2_global < b1 || 
                                              current_block->data->y1_global > b2;

                if (!current_block->data->ignore)
                {
                    printf("%d/%d: BLOCK COMM IN 1 i=%d --- %s a1=%d a2=%d b1=%d b2=%d / x1_local=%d x2_local=%d y1_local=%d y2_local=%d / x1_global=%d x2_global=%d y1_global=%d y2_global=%d\n", 
                                     my_facet, my_rank, i, sss, a1, a2, b1, b2, 
                                     current_block->data->x1_local,  current_block->data->x2_local,  current_block->data->y1_local,  current_block->data->y2_local,
                                     current_block->data->x1_global, current_block->data->x2_global, current_block->data->y1_global, current_block->data->y2_global);
                                     
                    // ajust the boundaries of the block.
                    current_block->data->a1 = current_block->data->x1_global < a1 ? current_block->data->x1_local + (a1 - current_block->data->x1_global) : current_block->data->x1_local; 
                    current_block->data->a2 = current_block->data->x2_global > a2 ? current_block->data->x2_local + (a2 - current_block->data->x2_global) : current_block->data->x2_local;
                    current_block->data->b1 = current_block->data->y1_global < b1 ? current_block->data->y1_local + (b1 - current_block->data->y1_global) : current_block->data->y1_local; 
                    current_block->data->b2 = current_block->data->y2_global > b2 ? current_block->data->y2_local + (b2 - current_block->data->y2_global) : current_block->data->y2_local;
                    
                    printf("%d/%d: BLOCK COMM IN 2 i=%d --- %s a1=%d a2=%d b1=%d b2=%d / x1_local=%d x2_local=%d y1_local=%d y2_local=%d / x1_global=%d x2_global=%d y1_global=%d y2_global=%d\n", 
                                     my_facet, my_rank, i, sss, current_block->data->a1, current_block->data->a2, current_block->data->b1, current_block->data->b2, 
                                     current_block->data->x1_local,  current_block->data->x2_local,  current_block->data->y1_local,  current_block->data->y2_local,
                                     current_block->data->x1_global, current_block->data->x2_global, current_block->data->y1_global, current_block->data->y2_global);
                }      
                else
                {
                    printf("%d/%d: BLOCK COMM OUT i=%d --- %s a1=%d a2=%d b1=%d b2=%d / x1_local=%d x2_local=%d y1_local=%d y2_local=%d / x1_global=%d x2_global=%d y1_global=%d y2_global=%d\n", 
                                     my_facet, my_rank, i, sss, a1, a2, b1, b2, 
                                     current_block->data->x1_local,  current_block->data->x2_local,  current_block->data->y1_local,  current_block->data->y2_local,
                                     current_block->data->x1_global, current_block->data->x2_global, current_block->data->y1_global, current_block->data->y2_global);
                                     
                    count += /* -= */ (current_block->data->x2_local - current_block->data->x1_local + 1) * (current_block->data->y2_local - current_block->data->y1_local + 1);   
                }

                current_block = current_block->next;
            }
            
            if (count > 0)
            {
                printf("%d/%d: HPCShelf_Oper - BEGIN (%d, %d, count=%d, (*config_block)[%d], b, dt, %d, %d, 2, %d);\n", my_facet, my_rank, P_node, N_node, count, i, partner_facet, i, col_port);
                // perform the operation with the adjusted blocks.
                HPCShelf_Oper(P_node, N_node, count, (*config_block)[i], b, dt, partner_facet, i, 2, col_port); 
                printf("%d/%d: HPCShelf_Oper - END (%d, %d, count=%d, (*config_block)[%d], b, dt, %d, %d, 2, %d); - END\n", my_facet, my_rank, P_node, N_node, count, i, partner_facet, i, col_port);
            }
            else
                printf("%d/%d: HPCShelf_Oper - NO (%d, %d, count=%d, (*config_block)[%d], b, dt, %d, %d, 2, %d);\n", my_facet, my_rank, P_node, N_node, count, i, partner_facet, i, col_port);
        }
    
    printf("%d/%d: END - GEMM INTER COMM PERFORM %s partner_facet=%d partner_size=%d\n", my_facet, my_rank, sss, partner_facet, partner_size); 
}

int GEMM_Recv (int P,
               int N,
               int count,
               BlockList* block,
               double *b,
	           MPI_Datatype datatype,
		       int facet,
		       int source,
		       int tag,
	           HPCShelf_Port port)
{
    if (block->data->a1 == 0 && 
        block->data->b1 == 0 && 
        block->data->a2 == P-1 && 
        block->data->b2 == N-1)
    {
       	HPCShelf_Recv(b, count, datatype, facet, source, tag, port);      
    }
    else
    {
	    double* buf;
        buf = (double*) malloc(count * sizeof(double)); 
	    HPCShelf_Recv(buf, count, datatype, facet, source, tag, port);  
	    
	    // printf("%d/%d: GEMM_Recv --- HPCShelfRecv --- count=%d facet=%d source=%d tag=%d port=%d\n", my_facet, my_rank, count, facet, source, tag, port);
	    
	    int l=0;
	    BlockList* current_block = block;
	    while (current_block)
	    {
            int x1 = current_block->data->a1;           
            int x2 = current_block->data->a2;
            int y1 = current_block->data->b1;
            int y2 = current_block->data->b2;
                
	        if (!current_block->data->ignore)
	        {   
                if (y1 == 0 && y2 == N-1)
                {
     	        //    printf("%d/%d: GEMM_Recv --- COPY TO BLOCK 1 --- x1=%d x2=%d y1=%d y2=%d\n", my_facet, my_rank, x1, x2, y1, y2);
     	            
                    int count_block = (x2 - x1 + 1)*N;
                    memcpy(b + x1*N, buf + l, count_block);
                    l += count_block;
                }
                else 
                {
     	         //   printf("%d/%d: GEMM_Recv --- COPY TO BLOCK 2 --- x1=%d x2=%d y1=%d y2=%d\n", my_facet, my_rank, x1, x2, y1, y2);
     	            
	                for (int i=x1; i<=x2; i++)
	                {
     	             //   printf("%d/%d: GEMM_Recv --- COPY TO BLOCK LINE --- x1=%d x2=%d i=%d N=%d l=%d\n", my_facet, my_rank, x1, x2, i, N, l);
     	            
	                	int size_block_line = y2 - y1 + 1;
	                    memcpy(b + i*N + y1, buf + l, size_block_line);
	                    l += size_block_line;
                        // for (int j=y1; j<=y2; j++)
	                    //    *(b + i*N + j) = *(buf + l++);
	                }
	            }
	        }
	        current_block = current_block->next;
        }
        
	    free(buf);
	}
}


int GEMM_Send (int P, 
               int N,
               int count,
               BlockList* block,
               double *b,
	           MPI_Datatype datatype,
		       int facet,
		       int target,
		       int tag,
	           HPCShelf_Port port)
{

    if (block->data->a1 == 0 && 
        block->data->b1 == 0 && 
        block->data->a2 == P-1 && 
        block->data->b2 == N-1)
    {
	    HPCShelf_Send(b, count, datatype, facet, target, tag, port);   
    }
    else
    {
	    double* buf;
	    buf = (double*) malloc(count * sizeof(double));
	    
	    int l=0;
	    BlockList* current_block = block;
	    while (current_block)
	    {
            int x1 = current_block->data->a1;
            int x2 = current_block->data->a2;
            int y1 = current_block->data->b1;
            int y2 = current_block->data->b2;
	        if (!current_block->data->ignore)
	        { 
                if (y1 == 0 && y2 == N-1)
                {
       	        //    printf("%d/%d: GEMM_Send --- COPY TO BLOCK 1 --- x1=%d x2=%d y1=%d y2=%d\n", my_facet, my_rank, x1, x2, y1, y2);
       	            
                    int size_block = (x2 - x1 + 1)*N;
                    memcpy(buf + l, b + x1*N, size_block);
                    l += size_block;
                }
                else 
                {
                //    printf("%d/%d: GEMM_Send --- COPY TO BLOCK 2 --- x1=%d x2=%d y1=%d y2=%d\n", my_facet, my_rank, x1, x2, y1, y2);
                
	                for (int i=x1; i<=x2; i++)
	                {
	                //    printf("%d/%d: GEMM_Send --- COPY FROM BLOCK LINE --- x1=%d x2=%d i=%d N=%d, l=%d\n", my_facet, my_rank, x1, x2, i, N, l);
	                
	                    int size_block_line = y2 - y1 + 1;
	                    memcpy(buf + l, b + i*N + y1, size_block_line);
	                    l += size_block_line;
	                    //for (int j=y1; j<=y2; j++)
		                //    *(buf + l++) = *(b + i*N + j);
	                }
	                
	            }
	        }
	        //else
	        //    printf("%d/%d: GEMM_Send --- IGNORE BLOCK --- x1=%d x2=%d y1=%d y2=%d\n", my_facet, my_rank, x1, x2, y1, y2);
	            
	        current_block = current_block->next;
        }
	    
	    printf("%d/%d: GEMM_Send BEGIN --- HPCShelfSend --- count=%d facet=%d target=%d tag=%d port=%d\n", my_facet, my_rank, count, facet, target, tag, port);
	    HPCShelf_Send(buf, count, datatype, facet, target, tag, port);   
	    printf("%d/%d: GEMM_Send END --- HPCShelfSend --- count=%d facet=%d target=%d tag=%d port=%d\n", my_facet, my_rank, count, facet, target, tag, port);
	    
	    free(buf);
	}
}

// assuming top->mc = top->ma e top->pc=top->pb
void gemm_inter_reduce_C (Topology* top_inter, 
                          Topology* top_intra,
                          int i,                         
                          int row,
                          int col, 
                          double alpha, 
                          double beta, 
                          double* c, 
                          double* c_prime, 
                          HPCShelf_Port row_port, 
                          FacetTopology* facet_topology)
{
  char message[256];

  printf("%d/%d: GEMM INTER REDUCE START\n", my_facet, my_rank);
  
  int M_node = top_intra->M/top_intra->X; 
  int N_node = top_intra->N/top_intra->Y; 
  int P_node_x = top_intra->P/top_intra->X; 
  int P_node_y = top_intra->P/top_intra->Y;         //60
    
  int facet_count = facet_topology->facet_count;
  int* facet_size = facet_topology->facet_size; 
  int* facet_instance = facet_topology->facet_instance;

  int block_iterations = top_inter->P / (top_inter->pb * top_inter->X);

  for (int j=0; j<block_iterations; j++)
  {
     int root = ((i + row) % top_inter->X + j*top_inter->X) % top_inter->Y;
  
     sprintf(message, "GEMM INTERCLUSTER/REDUCE 1 rank=%d i=%d j=%d root=%d row=%d col=%d me=%d", my_rank, i, j, root, row, col, *(facet_instance + col));  HPCShelf_Browse(message); 
  
     if (col == root)
     {
         double* c_j = c + ((int)((abs(i - row) + j*top_inter->X)/block_iterations))*top_inter->pb;
//         printf("%d/%d: ccccc[%d]\n", my_facet, my_rank, ((int)((abs(i - row) + j*top_inter->X)/block_iterations /*top_inter->Y*/))*top_inter->pb);
         
         // accumulate c_prime to c
         for (int a=0; a<M_node; a++)
             for (int b=0; b<top_intra->pb; b++)
                 *(c_j + a*P_node_y + b) += *(c_prime + a*P_node_x + b);

         int partner = (root + 1) % top_inter->Y;        
         
         while (partner != root)
         {
            int source = facet_instance[partner];
            sprintf(message, "GEMM INTERCLUSTER/REDUCE RECV 2 rank=%d i=%d j=%d root=%d row=%d col=%d partner=%d source=%d me=%d", my_rank, i, j, root, row, col, partner, source, *(facet_instance + col));  HPCShelf_Browse(message); 
    	    gemm_inter_comm_perform ("ROOT", &GEMM_Recv, M_node, P_node_x, c_prime, MPI_DOUBLE, facet_instance[partner], facet_size[partner], row_port, &config_block_r[partner], 0, M_node-1, j*top_inter->pb, (j+1)*top_inter->pb-1);
            sprintf(message, "GEMM INTERCLUSTER/REDUCE RECV 3 rank=%d i=%d j=%d root=%d row=%d col=%d partner=%d source=%d me=%d", my_rank, i, j, root, row, col, partner, source, *(facet_instance + col));  HPCShelf_Browse(message); 
                        
            // accumulate c_prime_parther to c
	    	for (int a=0; a<M_node; a++)
	        	for (int b=0; b<top_intra->pb; b++)
	           		*(c_j + a*P_node_y + b) += *(c_prime + a*P_node_x + b);
		        
            partner = (partner + 1) % top_inter->Y;          
         }
     }
     else
     {
         int target = facet_instance[root];
         sprintf(message, "GEMM INTERCLUSTER/REDUCE SEND 2 rank=%d i=%d j=%d root=%d row=%d col=%d target=%d me=%d", my_rank, i, j, root, row, col, target, *(facet_instance + col));  HPCShelf_Browse(message); 
    	 gemm_inter_comm_perform ("PEER", &GEMM_Send, M_node, P_node_x, c_prime, MPI_DOUBLE, facet_instance[root], facet_size[root], row_port, &config_block_r[root], 0, M_node-1, j*top_inter->pb, (j+1)*top_inter->pb-1);
         sprintf(message, "GEMM INTERCLUSTER/REDUCE SEND 3 rank=%d i=%d j=%d root=%d row=%d col=%d target=%d me=%d", my_rank, i, j, root, row, col, target, *(facet_instance + col));  HPCShelf_Browse(message); 
     }
     //k = k+1 % top_inter->Y;
  }
  
  sprintf(message, "GEMM INTERCLUSTER/REDUCE 4 rank=%d i=%d row=%d col=%d me=%d", my_rank, i, row, col, *(facet_instance + col));  HPCShelf_Browse(message); 
} 


void gemm_inter_rotate_B (Topology* top_inter, 
                          Topology* top_intra, 
                          int i, 
                          int row, 
                          int col, 
                          double **b, 
                          double **b_prime, 
                          HPCShelf_Port col_port, 
                          FacetTopology* facet_topology)
{    
    int facet_count = facet_topology->facet_count;
    int* facet_size = facet_topology->facet_size; 
    int* facet_instance = facet_topology->facet_instance;

    int N_node = top_intra->N/top_intra->Y; //60
    int P_node = top_intra->P/top_intra->X; //60

    int target_row = row == top_inter->X-1 ? 0              : row + 1;
    int source_row = row == 0              ? top_inter->X-1 : row - 1;

    #pragma omp parallel sections num_threads(2)
    {      
      #pragma omp section
      { 
        gemm_inter_comm_perform("SEND", &GEMM_Send, P_node, N_node, *b      , MPI_DOUBLE, facet_instance[target_row], facet_size[target_row], col_port, &config_block_c[target_row], 0, top_intra->P-1, 0, top_intra->N-1);
      }
      #pragma omp section
      {
        gemm_inter_comm_perform("RECV", &GEMM_Recv, P_node, N_node, *b_prime, MPI_DOUBLE, facet_instance[source_row], facet_size[source_row], col_port, &config_block_c[source_row], 0, top_intra->P-1, 0, top_intra->N-1);
      }
    }
    
    // swap b and b_prime
    double** temp = b;      
    b = b_prime;
    b_prime = temp;    
}








