
typedef struct BlockConfigType
{
  int x1_local;
  int x2_local;
  int y1_local;
  int y2_local;
  int x1_global;
  int x2_global;
  int y1_global;
  int y2_global;
  int ignore;
  int a1;
  int a2;
  int b1;
  int b2;
} BlockConfig;

typedef struct BlockListType
{
   BlockConfig* data;
   struct BlockListType* next;
} BlockList;

BlockList* insert(BlockList* list, void* data);
