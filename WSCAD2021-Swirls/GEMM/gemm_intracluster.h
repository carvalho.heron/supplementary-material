#include "mpi.h"
#include "topology.h"

void gemm_intracluster(int facet_world,
                       double  alpha, 
                       double* a, 
                       double* b, 
                       double  beta, 
                       double* c, 
                       double* c_prime, 
                       Topology* top, 
                       MPI_Comm comm);
