#include <stdlib.h>
#include "hpcshelf.h"
#include "facet_topology.h"


FacetTopology* getFacetTopology(char* port_name)
{
   FacetTopology* facet_topology = (FacetTopology*) malloc(sizeof(FacetTopology));
  
   HPCShelf_Get_port(port_name, &facet_topology->port);

   HPCShelf_Get_facet(facet_topology->port, &facet_topology->facet);
   
   HPCShelf_Get_facet_count(facet_topology->port, &facet_topology->facet_count);
   
   facet_topology->facet_size = (int*) malloc(facet_topology->facet_count * sizeof(int));
   HPCShelf_Get_facet_size(facet_topology->port, facet_topology->facet_size);

   facet_topology->facet_instance = (int*) malloc(facet_topology->facet_count * sizeof(int));
   HPCShelf_Get_facet_instance(facet_topology->port, facet_topology->facet_instance);

   return facet_topology;
}

void releasePort(FacetTopology* facet_topology)
{
   HPCShelf_Release_port(facet_topology->port);
   free(facet_topology->facet_size);
   free(facet_topology->facet_instance);
}
