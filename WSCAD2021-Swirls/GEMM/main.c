#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <malloc.h>
#include <time.h>
#include "mpi.h"
#include "hpcshelf.h"
#include "gemm_intercluster.h"

void fill_data(double* a, double* b, double* c, int M, int N, int P_x, int P_y);

int main(int argc, char *argv[]) 
{
    int rank;
    char message[256];	

    MPI_Init(&argc, &argv);
    
    HPCShelf_Init(&argc, &argv);
    
    int facet_world;
    HPCShelf_Get_facet_world(&facet_world);
    
//    int facet_size_world;
//    HPCShelf_Get_facet_size_world(&facet_size_world);    
    
    MPI_Comm comm = MPI_COMM_WORLD;
  //  MPI_Comm_get_parent(&comm);
    MPI_Comm_rank(comm, &rank);
    
    printf("%d MAIN ==================================== FACET WORLD = %d\n", rank, facet_world);

    Topology* top_inter = (Topology*) malloc(sizeof(Topology));
    Topology* top_intra = (Topology*) malloc(sizeof(Topology));   

    readTopologyG(top_inter);
    readTopologyL(facet_world, top_intra);

    //if (rank == 0)
    //{
    //   char topology_string[strlen("topology_???.txt") + 1];
    //   sprintf(topology_string, "topology_%d.txt", facet_world);

    //   readTopology(topology_string, top_intra);
    //   readTopologyG("topology.txt", top_inter);
       
    //   printf("%d/%d: BROADCAST TOPOLOGY (root) %d !!!\n", facet_world, rank, (int) sizeof(Topology));
    //}
    //else
    //   printf("%d/%d: BROADCAST TOPOLOGY (peer) %d !!!\n", facet_world, rank, (int) sizeof(Topology));
    
    // MPI_Bcast(top_intra, 10, MPI_INT, 0, comm);   
    // MPI_Bcast(top_inter, 10, MPI_INT, 0, comm);   
           
    int M_node = top_intra->M/top_intra->X;         //40
    int N_node = top_intra->N/top_intra->Y;         //60
    int P_node_x = top_intra->P/top_intra->X;         //60
    int P_node_y = top_intra->P/top_intra->Y;         //60
   
    sprintf(message, "%d/%d: GEMM 1 M_node=%d, N_node=%d P_node_x=%d P_node_y=%d", facet_world, rank, M_node, N_node, P_node_x, P_node_y);
    HPCShelf_Browse(message); 

    int a_size = M_node*N_node*sizeof(double);
    int b0_size = P_node_x*N_node*sizeof(double);
    int b0_prime_size = P_node_x*N_node*sizeof(double);
    int c_prime_size = M_node * P_node_x * sizeof(double);
    int c_size = M_node * P_node_y * sizeof(double);  
    
    int total_size = a_size + b0_size + b0_prime_size + c_prime_size + c_size;
    
    sprintf(message, "%d/%d: MEMORY SIZE = %d", facet_world, rank, total_size);
    HPCShelf_Browse(message); 

    double  alpha = 3.0;  // constant
    double* a = (double*) malloc(a_size); // A blocks 
    double* b0 = (double*) malloc(b0_size);
    double** b = &b0;
    double* b0_prime = (double*) malloc(b0_prime_size);    
    double** b_prime = &b0_prime; 
    double  beta = 2.0;   // constant
    double* c_prime = (double*) malloc(c_prime_size);
    double* c = (double*) malloc(c_size); // C blocks

    sprintf(message, "%d/%d: TOPOLOGY GLOBAL X=%d, Y=%d, M=%d, N=%d, P=%d, ma=%d, n=%d, pb=%d, mc=%d, pc=%d\n", facet_world, rank, top_inter->X, top_inter->Y, top_inter->M, top_inter->N, top_inter->P, top_inter->ma, top_inter->n, top_inter->pb, top_inter->mc, top_inter->pc);
    HPCShelf_Browse(message);
    
    sprintf(message,"%d/%d: TOPOLOGY LOCAL X=%d, Y=%d, M=%d, N=%d, P=%d, ma=%d, n=%d, pb=%d, mc=%d, pc=%d\n", facet_world, rank, top_intra->X, top_intra->Y, top_intra->M, top_intra->N, top_intra->P, top_intra->ma, top_intra->n, top_intra->pb, top_intra->mc, top_intra->pc);
    HPCShelf_Browse(message);

    fill_data(a, *b, c, M_node, N_node, P_node_x, P_node_y);

    gemm_intercluster(alpha, a, b, b_prime, beta, c, c_prime, top_intra, top_inter);
/*
    for (int i = 0; i < M_node; i++) {
        for (int j = 0; j < top_intra->P/top_intra->Y; j++) {
            sprintf(message, "%d: c[%d,%d]=%f\n", rank, i, j, *(c + (P_node * i) + j));
            HPCShelf_Browse(message);
        }
    }      
  */  

    HPCShelf_Finalize();

    MPI_Finalize();
    
   
    return 0;
}



void fill_data(double* a, double* b, double* c, int M, int N, int Px ,int Py)
{
    int i, j;

    srand(time(NULL));

    for (i = 0; i < M; i++) {
        for (j = 0; j < N; j++) {
            *(a + N*i + j) = 1; //(double)rand()/RAND_MAX;
        }
    }

    for (i = 0; i < Px; i++) {
        for (j = 0; j < N; j++) {
            *(b + N*i + j) = 1; //(double)rand()/RAND_MAX;
        }
    }

    for (i = 0; i < M; i++) {
        for (j = 0; j < Py; j++) {
            *(c + Py*i + j) = 1; //(double)rand()/RAND_MAX;
        }
    }
}
