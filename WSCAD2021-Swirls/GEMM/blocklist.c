#include <stddef.h>
#include <stdlib.h>
#include "blocklist.h"

BlockList* insert(BlockList* list, void* data)
{
    BlockList* next = (BlockList*) malloc(sizeof(BlockList));
    next->data = data;
    next->next = NULL;
 
    if (list == NULL)
        return next;
    else
    {
        BlockList* l = list;
        while (l->next != NULL) l = l->next;        
        l->next = next;
        return list;
    }
}
