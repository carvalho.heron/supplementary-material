#include "topology.h"
#include <stdio.h>
#include <stdlib.h>

#define MAXTOP 100 //A função fgets necessita de um buffer


void readTopologyG(Topology* top)
{
    top->X  = 2;
    top->Y  = 2;
    top->M  = 160;
    top->N  = 360; //240;
    top->P  = 240; //200;
    top->ma = 20;
    top->n  = 20;
    top->pb = 20;
    top->mc = 20;
    top->pc = 20; 
    

}

void readTopologyL(int facet_world, Topology* top)
{
    switch (facet_world)
    {
        case 1: 
                top->X  =  2;
                top->Y  =  2;
                top->M  =  80;
                top->N  =  180;
                top->P  =  120;
                top->ma =  20;
                top->n  =  30;
                top->pb =  20;
                top->mc =  20;
                top->pc =  20;
                break;
        case 2:
                top->X  =  2;
                top->Y  =  3;
                top->M  =  80;
                top->N  =  180;
                top->P  =  120;
                top->ma =  20;
                top->n  =  20;
                top->pb =  20;
                top->mc =  20;
                top->pc =  20;
                break;
        case 3:
                top->X  =  2;
                top->Y  =  3;
                top->M  =  80;
                top->N  =  180;
                top->P  =  120;
                top->ma =  20;
                top->n  =  20;
                top->pb =  20;
                top->mc =  20;
                top->pc =  20;
                break;
        case 4:
                top->X  =  2;
                top->Y  =  2;
                top->M  =  80;
                top->N  =  180;
                top->P  =  120;
                top->ma =  20;
                top->n  =  30;
                top->pb =  20;
                top->mc =  20;
                top->pc =  20;
                break;
    
    }

}

void readTopology(char* input_file, Topology* top)
{

    FILE* arquivo;
    char *c;
    int i;

    printf("1 - READING TOPOlOGY FILE %s\n", input_file);

    arquivo = fopen(input_file, "r");
    
    printf("2 - READING TOPOlOGY FILE %s\n", input_file);
    
    if (arquivo == NULL) {
        printf("Não foi possível configurar a topologia!\n");
        exit(EXIT_FAILURE);
    }
    i = -1;
    
    printf("3 - READING TOPOlOGY FILE %s\n", input_file);
    while (!feof(arquivo)) 
    {
        printf("4 - READING TOPOlOGY FILE %s\n", input_file);
        char* r = fgets(c, MAXTOP, arquivo);
        if (r == NULL)
        {
           printf("Topology read error\n");
           break;        
        }
        i++;
        printf("5 - READING TOPOlOGY FILE %s\n", input_file);
        switch (i) {
            case 0:
                top->X =  atoi(c);
                printf("top->X = %d\n", top->X);
                continue;
            case 1:
                top->Y =  atoi(c);
                printf("top->Y = %d\n", top->Y);
                continue;
            case 2:
                top->M =  atoi(c);
                printf("top->M = %d\n", top->M);
                continue;
            case 3:
                top->N =  atoi(c);
                printf("top->N = %d\n", top->N);
                continue;
            case 4:
                top->P =  atoi(c);
                printf("top->P = %d\n", top->P);
                continue;
            case 5:
                top->ma =  atoi(c);
                printf("top->ma = %d\n", top->ma);
                continue;
            case 6:
                top->n =  atoi(c);
                printf("top->n = %d\n", top->n);
               continue;
            case 7:
                top->pb =  atoi(c);
                printf("top->pb = %d\n", top->pb);
                continue;
            case 8:
                top->mc =  atoi(c);
                printf("top->mc = %d\n", top->mc);
                continue;
            case 9:
                top->pc =  atoi(c);
                printf("top->pc = %d\n", top->pc);
                continue;
        }
    }

    printf("6 - READING TOPOlOGY FILE %s\n", input_file);

    fclose(arquivo);
}
