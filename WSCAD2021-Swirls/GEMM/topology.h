
#ifndef _TOPOLOGY_H_
#define _TOPOLOGY_H_

typedef struct TopologyType 
{
  int X, Y;              // process grid dimensions
  int M, N, P;           // total matrix dimensions (A(MxN), B(NxP), C(MxP))
  int ma, n, pb, mc, pc; // block dimensions (A(ma x n), B(n x pb), C(mc x pc))
} Topology;

#define MAXTOP 100 //A função fgets necessita de um buffer

void readTopology(char* input_file, Topology* top);
void readTopologyG(Topology* top);
void readTopologyL(int facet_world, Topology* top);

#endif
